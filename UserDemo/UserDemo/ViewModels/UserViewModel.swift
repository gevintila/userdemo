//
//  UserViewModel.swift
//  UserDemo
//
//  Created by Georgel Vintila on 16.05.2022.
//

import UIKit


struct UserViewModel {
    
    var name:String
    var initial:String
    var email:String
    var isFavorite:Bool
    var imageURL:URL?
    var date:Date
    
    
    init(user:User) {
        name = "\(user.name.first) \(user.name.last)"
        initial = String(user.name.first.first ?? "_")
        email = user.email
        isFavorite = false //no actual use for this
        imageURL = user.picture?.thumbnail //use smallest image to reduce image load and storage size
        date = Date() //don't actualy know what the time represents, there is no actual date in the User model to match it
    }
    
    
    var time:String {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        return formatter.string(from: date)
        
    }
    
    
    
    func loadImage(block:((UIImage?)->Void)? = nil) { //make the block option for prefetching
        
        guard let url = imageURL else {
            block?(nil)
            return
        }
        if let image = ImageCache.cache.fetch(key: url.absoluteString) {
            block?(image)
            return
        }
        
        DispatchQueue(label: "GetImage").async {
            if let data = try? Data(contentsOf: url) {
                let image = UIImage(data: data)
                DispatchQueue.main.async {
                    block?(image)
                    ImageCache.cache.store(image: image, key: url.absoluteString)//store the images in a cache storage for quick access later
                }
            }
            
        }
    }
    
}
