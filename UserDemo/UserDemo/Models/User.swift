//
//  User.swift
//  UserDemo
//
//  Created by Georgel Vintila on 16.05.2022.
//

import Foundation

// the full user model sent by the server

struct User:Codable {

    struct Name:Codable{
        let title:String?
        let first:String
        let last:String
    }
    
    struct Location:Codable {
        
        struct Street:Codable {
            let number:Int
            let name:String
        }
        
        struct Coords:Codable{
            let latitude:String
            let longitude:String
        }
        
        struct Timezone:Codable {
            let offset:String
            let description:String
        }
        
        let street:Street?
        let city:String?
        let state:String?
        let country:String?
//        let postcode:Int?
        let coordinates:Coords?
        let timezone:Timezone?
    }
    
    struct LoginInfo:Codable{
        let uuid:String
        let username:String
        let password:String
        let salt:String
        let md5:String
        let sha1:String
        let sha256:String
    }
    
    struct DateInfo:Codable{
        let date:String
        let age:Int
    }
    
    struct IdInfo:Codable {
        let name:String
        let value:String
    }
    
    struct Picture:Codable {
        let large:URL
        let medium:URL
        let thumbnail:URL
    }
    

    let gender:String?
    let name:Name
    let location:Location?
    let email:String
    let login:LoginInfo?
    let dob:DateInfo?
    let registered:DateInfo
    let phone:String?
    let cell:String?
//    let id:IdInfo?
    let picture:Picture?
    let nat:String?
            
}
