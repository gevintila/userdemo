//
//  Result.swift
//  UserDemo
//
//  Created by Georgel Vintila on 19.05.2022.
//

import Foundation

//created to add a wrapper over the result to match the data received from the server
struct Result<T>:Codable where T:Codable {
    let results:[T]
}
