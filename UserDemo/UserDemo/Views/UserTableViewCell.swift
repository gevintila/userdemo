//
//  UserTableViewCell.swift
//  UserDemo
//
//  Created by Georgel Vintila on 16.05.2022.
//

import UIKit

class UserTableViewCell: UITableViewCell {

    @IBOutlet var nameLabel:UILabel!
    @IBOutlet var initalLabel:UILabel!
    @IBOutlet var emailLabel:UILabel!
    @IBOutlet var timeLabel:UILabel!
    @IBOutlet var favoriteButton:UIButton!
    @IBOutlet var iconImageView:UIImageView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        iconImageView.image = nil
    }

    
    func load(user:UserViewModel) {
        nameLabel.text = user.name
        initalLabel.text = user.initial
        emailLabel.text = user.email
        timeLabel.text = user.time
        favoriteButton.isSelected = user.isFavorite
        user.loadImage { [weak self] image in
            self?.iconImageView.image = image
        }
    }
    
    @IBAction func favoritePressed(_ sender:UIButton) {
        sender.isSelected = !sender.isSelected
    }

}
