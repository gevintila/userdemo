//
//  ImageCache.swift
//  UserDemo
//
//  Created by Georgel Vintila on 19.05.2022.
//

import UIKit

class ImageCache {
    
    private var storage:[String:UIImage] = [:]
    
    static let cache = ImageCache()
    
    func store(image:UIImage?, key:String) {
        if let image = image {
            storage[key] = image
        } else {
            storage.removeValue(forKey: key)
        }
    }
    
    func fetch(key:String) -> UIImage? {
        return storage[key]
    }
}
