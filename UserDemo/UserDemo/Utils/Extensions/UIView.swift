//
//  UIView.swift
//  UserDemo
//
//  Created by Georgel Vintila on 16.05.2022.
//

import UIKit

extension UIView {
    
    @IBInspectable var cornerRadius:CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
}
