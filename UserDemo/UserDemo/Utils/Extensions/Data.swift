//
//  Data.swift
//  UserDemo
//
//  Created by Georgel Vintila on 19.05.2022.
//

import Foundation

extension Data {
    func decode<T>(_ type: T.Type) -> T? where T : Decodable {
        do {
            return try JSONDecoder().decode(type, from: self)
        } catch {
//            print(content)
            print(error)
        }
        return nil
    }
    
    var content:String {
        return String(data: self, encoding: .utf8) ?? "<nil>"
    }
    
}
