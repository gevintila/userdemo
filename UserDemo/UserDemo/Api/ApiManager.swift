//
//  ApiManager.swift
//  UserDemo
//
//  Created by Georgel Vintila on 19.05.2022.
//

import Foundation


let baseUrl = "https://randomuser.me/api"

typealias ApiParams = [String:Any]

enum Endpoints:String {
    case getUsers = "/"
    
    func path(params:String = "") -> String {
        return rawValue.appending("?\(params)")
    }
}


class ApiManager {
    
    func fetchUsers(params:ApiParams, result: @escaping ([User])->Void) {
        
        //allows the customisation of endpoints with a variable number of parameters
        let listParams = params.map{ "\($0.key)=\($0.value)" }
        let endpoint = Endpoints.getUsers.path(params: listParams.joined(separator: "&"))
        let path = baseUrl.appending(endpoint)
        
        if let url = URL(string: path) {
            let task = dataTask(url: url) { data in
                let list = data?.decode(Result<User>.self)?.results ?? []
                result(list)
            }
            task.resume()
        }
        
    }
    
    
    func dataTask(url:URL, result: @escaping ((Data?)->Void)) -> URLSessionDataTask {
        return URLSession(configuration: .default).dataTask(with: url) { data, response, error in
            if let status = (response as? HTTPURLResponse)?.statusCode {
                switch status {
                    case 200..<300:
                        result(data)
                    default:
                        result(nil)
                    }
            } else {
                result(nil)
            }
        }
    }
    
}
