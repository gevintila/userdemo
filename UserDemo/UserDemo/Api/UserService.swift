//
//  UserService.swift
//  UserDemo
//
//  Created by Georgel Vintila on 19.05.2022.
//

import Foundation

// an intermediary layer between the api manager and the view controller in order to convert the data into the correct format
class UserService {
    
    let manager = ApiManager()
    
    func fetchUsers(page:Int, count:Int, seed:String = "abc", result: @escaping ([UserViewModel])->Void) {
        var params = ApiParams()
        params["page"] = page
        params["results"] = count
        params["seed"] = seed
        
        manager.fetchUsers(params: params) { users in
            result( users.map{ UserViewModel(user: $0) } )
        }
    }
    
}
