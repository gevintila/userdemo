//
//  ViewController.swift
//  UserDemo
//
//  Created by Georgel Vintila on 16.05.2022.
//

import UIKit

let maxPage = 3
let itemsPerPage = 20

class ViewController: UIViewController {

    @IBOutlet var tableView:UITableView!
    
    var currentPage = 0
    
    var users:[UserViewModel] = []
    var service = UserService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        load(page: 1)
    }

    func load(page:Int) {
        //making sure the page is not loaded twice
        if currentPage < page && currentPage < maxPage{
            service.fetchUsers(page: currentPage, count: itemsPerPage) { [weak self] users in
                self?.append(items:users)
            }
            currentPage += 1
        }
    }
    
    func append(items:[UserViewModel]) {
        let start = users.count
        let indexList = Array((start..<start+items.count).map{ IndexPath(row: $0, section: 0) })

        DispatchQueue.main.async {
            self.tableView.performBatchUpdates { [weak self] in
                self?.users.insert(contentsOf:items, at: start)
                self?.tableView.insertRows(at: indexList, with: .automatic)
            }
        }
        
    }
}


extension ViewController:UITableViewDelegate, UITableViewDataSource, UITableViewDataSourcePrefetching {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell", for: indexPath) as! UserTableViewCell
        let user = users[indexPath.item]
        cell.load(user: user)
        cell.initalLabel.text = "\(indexPath.row)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == users.count - 4 {
            load(page: currentPage + 1)
        }
    }
    
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        for indexPath in indexPaths {
            let user = users[indexPath.item]
            user.loadImage()
        }
        
    }
}
